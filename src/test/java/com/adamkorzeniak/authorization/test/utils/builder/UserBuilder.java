package com.adamkorzeniak.authorization.test.utils.builder;

import com.adamkorzeniak.authorization.domain.model.User;
import com.adamkorzeniak.authorization.test.utils.TestData;
import com.adamkorzeniak.utils.security.infrastructure.model.Role;

public class UserBuilder {
    protected Long id;
    private String username;
    private String password;
    private Role role;
    private boolean enabled;

    public static UserBuilder sample() {
        UserBuilder userBuilder = new UserBuilder();
        userBuilder.id = 123L;
        userBuilder.username = "admin";
        userBuilder.password = TestData.PASSWORD;
        userBuilder.role = Role.ADMIN;
        userBuilder.enabled = true;
        return userBuilder;
    }

    public UserBuilder id(Long id) {
        this.id = id;
        return this;
    }

    public UserBuilder encode() {
        this.password = TestData.ENCODED_PASSWORD;
        return this;
    }

    public User build() {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setRole(role);
        user.setEnabled(enabled);
        return user;
    }

}
