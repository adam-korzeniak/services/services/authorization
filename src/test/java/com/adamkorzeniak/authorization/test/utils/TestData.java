package com.adamkorzeniak.authorization.test.utils;

public class TestData {
    public static final String ADMIN_USERNAME = "admin";
    public static final String ADMIN_ROLE = "ADMIN";
    public static final String USER_USERNAME = "user";
    public static final String USER_ROLE = "USER";
    public static final String NEW_USER_USERNAME = "newUser";

    public static final String PASSWORD = "password";
    public static final String ADMIN_PASSWORD = "admin";
    public static final String ENCODED_PASSWORD = "pa$$W0rd";
}
