package com.adamkorzeniak.authorization.test.utils.builder;

import com.adamkorzeniak.authorization.api.rest.model.UserRequest;
import com.adamkorzeniak.utils.security.infrastructure.model.Role;

public class UserRequestBuilder {
    private String username;
    private String password;
    private String role;

    public static UserRequestBuilder sample() {
        UserRequestBuilder userBuilder = new UserRequestBuilder();
        userBuilder.username = "admin";
        userBuilder.password = "password";
        userBuilder.role = Role.ADMIN.name();
        return userBuilder;
    }

    public UserRequestBuilder username(String username) {
        this.username = username;
        return this;
    }

    public UserRequestBuilder role(String role) {
        this.role = role;
        return this;
    }

    public UserRequest build() {
        return new UserRequest(username, password, role);
    }

}
