package com.adamkorzeniak.authorization.test.utils;

import static com.adamkorzeniak.authorization.test.utils.TestData.ADMIN_USERNAME;
import static com.adamkorzeniak.authorization.test.utils.TestData.USER_USERNAME;

public class Messages {
    public static final String USER_DUPLICATE_CODE = "AUTH__USER_DUPLICATE";
    public static final String USER_DUPLICATE_TITLE = "Username already taken";
    public static final String DUPLICATE_USER_MESSAGE = "User with username %s already exists";
    public static final String DUPLICATE_ADMIN_USER_MESSAGE = String.format(DUPLICATE_USER_MESSAGE, ADMIN_USERNAME);
    public static final String DUPLICATE_USER_USER_MESSAGE = String.format(DUPLICATE_USER_MESSAGE, USER_USERNAME);
}
