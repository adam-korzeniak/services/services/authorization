package com.adamkorzeniak.authorization.test.utils;

public class Urls {
    public static final String GET_ME_PATH = "/api/user/me";
    public static final String REGISTER_USER_PATH = "/api/user/register";
}
