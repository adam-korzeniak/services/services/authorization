package com.adamkorzeniak.authorization.integration;

import com.adamkorzeniak.authorization.api.rest.model.UserRequest;
import com.adamkorzeniak.authorization.test.utils.Messages;
import com.adamkorzeniak.authorization.test.utils.builder.UserRequestBuilder;
import com.adamkorzeniak.utils.security.infrastructure.model.Role;
import com.adamkorzeniak.utils.security.infrastructure.persistence.UserRepository;
import com.adamkorzeniak.utils.security.infrastructure.persistence.model.UserEntity;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static com.adamkorzeniak.authorization.test.utils.TestData.*;
import static com.adamkorzeniak.authorization.test.utils.Urls.GET_ME_PATH;
import static com.adamkorzeniak.authorization.test.utils.Urls.REGISTER_USER_PATH;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;


    @BeforeEach
    public void setup() {
        userRepository.save(buildEntity(1, USER_USERNAME, PASSWORD, Role.USER));
        userRepository.save(buildEntity(2, ADMIN_USERNAME, ADMIN_PASSWORD, Role.ADMIN));
    }

    @AfterEach
    public void cleanup() {
        userRepository.deleteAll();
    }

    @Test
    @WithMockUser(username = USER_USERNAME, password = PASSWORD)
    void GetUserDetails_UserFound_ReturnsUser() throws Exception {
        mockMvc.perform(get(GET_ME_PATH))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.username", is(USER_USERNAME)))
                .andExpect(jsonPath("$.role", is(USER_ROLE)))
                .andExpect(jsonPath("$.id").doesNotExist())
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.enabled").doesNotExist());
    }

    @Test
    @WithMockUser(username = "userX", password = PASSWORD)
    void GetUserDetails_UserNotFound_NoContentReturned() throws Exception {
        mockMvc.perform(get(GET_ME_PATH))
                .andExpect(status().isNoContent())
                .andExpect(jsonPath("$").doesNotExist());
    }

    @Test
    @WithMockUser(username = ADMIN_USERNAME, password = ADMIN_PASSWORD, roles = ADMIN_ROLE)
    void RegisterUser_NoIssues_RegisteredUserReturned() throws Exception {
        UserRequest request = UserRequestBuilder.sample().username(NEW_USER_USERNAME).build();
        String requestJson = objectMapper.writeValueAsString(request);

        mockMvc.perform(post(REGISTER_USER_PATH).content(requestJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username", is(NEW_USER_USERNAME)))
                .andExpect(jsonPath("$.role", is(ADMIN_ROLE)))
                .andExpect(jsonPath("$.id").doesNotExist())
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.enabled").doesNotExist());

        assertThat(userRepository.findAll()).hasSize(3);

        Optional<UserEntity> result = userRepository.findByUsername(NEW_USER_USERNAME);
        assertThat(result).isNotEmpty();

        UserEntity newUser = result.get();
        assertThat(newUser.getId()).isNotNull();
        assertThat(newUser.getUsername()).isEqualTo(NEW_USER_USERNAME);
        assertThat(newUser.getRole()).isEqualTo(Role.ADMIN);
        assertThat(passwordEncoder.matches(PASSWORD, newUser.getPassword())).isTrue();
    }

    @Test
    @WithMockUser(username = ADMIN_USERNAME, password = ADMIN_PASSWORD, roles = ADMIN_ROLE)
    void RegisterUser_NoRoleProvided_RegisteredUserWithDefaultRole() throws Exception {
        UserRequest request = UserRequestBuilder.sample().username(NEW_USER_USERNAME).role(null).build();
        String requestJson = objectMapper.writeValueAsString(request);

        mockMvc.perform(post(REGISTER_USER_PATH).content(requestJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username", is(NEW_USER_USERNAME)))
                .andExpect(jsonPath("$.role", is(USER_ROLE)))
                .andExpect(jsonPath("$.id").doesNotExist())
                .andExpect(jsonPath("$.password").doesNotExist())
                .andExpect(jsonPath("$.enabled").doesNotExist())
                .andReturn();

        assertThat(userRepository.findAll()).hasSize(3);

        Optional<UserEntity> result = userRepository.findByUsername(NEW_USER_USERNAME);
        assertThat(result).isNotEmpty();

        UserEntity newUser = result.get();
        assertThat(newUser.getId()).isNotNull();
        assertThat(newUser.getUsername()).isEqualTo(NEW_USER_USERNAME);
        assertThat(newUser.getRole()).isEqualTo(Role.USER);
        assertThat(passwordEncoder.matches(PASSWORD, newUser.getPassword())).isTrue();
    }

    @Test
    @WithMockUser(username = ADMIN_USERNAME, password = ADMIN_PASSWORD, roles = ADMIN_ROLE)
    void RegisterUser_DuplicatedUser_ReturnsException() throws Exception {
        UserRequest request = UserRequestBuilder.sample().username(USER_USERNAME).build();
        String requestJson = objectMapper.writeValueAsString(request);

        mockMvc.perform(post(REGISTER_USER_PATH).content(requestJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errorId", notNullValue()))
                .andExpect(jsonPath("timestamp", notNullValue()))
                .andExpect(jsonPath("errors", hasSize(1)))
                .andExpect(jsonPath("errors[0].code", is(Messages.USER_DUPLICATE_CODE)))
                .andExpect(jsonPath("errors[0].title", is(Messages.USER_DUPLICATE_TITLE)))
                .andExpect(jsonPath("errors[0].message", is(Messages.DUPLICATE_USER_USER_MESSAGE)));

        assertThat(userRepository.findAll()).hasSize(2);
    }

    @Test
    @WithMockUser(username = USER_USERNAME, password = PASSWORD)
    void RegisterUser_NotAdmin_ReturnsAccessDenied() throws Exception {
        UserRequest request = UserRequestBuilder.sample().username(NEW_USER_USERNAME).build();
        String requestJson = objectMapper.writeValueAsString(request);

        mockMvc.perform(post(REGISTER_USER_PATH).content(requestJson).contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden());

        assertThat(userRepository.findAll()).hasSize(2);

        Optional<UserEntity> result = userRepository.findByUsername(NEW_USER_USERNAME);
        assertThat(result).isEmpty();
    }

    private UserEntity buildEntity(long id, String username, String password, Role role) {
        var user = new UserEntity();
        user.setId(id);
        user.setUsername(username);
        user.setPassword(passwordEncoder.encode(password));
        user.setRole(role);
        return user;
    }
}
