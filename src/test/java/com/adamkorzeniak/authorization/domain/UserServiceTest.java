package com.adamkorzeniak.authorization.domain;

import com.adamkorzeniak.authorization.domain.exception.model.UserDuplicateException;
import com.adamkorzeniak.authorization.domain.model.User;
import com.adamkorzeniak.authorization.infrastructure.persistence.UserPersistenceService;
import com.adamkorzeniak.authorization.test.utils.builder.UserBuilder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.Principal;
import java.util.Optional;

import static com.adamkorzeniak.authorization.test.utils.Messages.DUPLICATE_ADMIN_USER_MESSAGE;
import static com.adamkorzeniak.authorization.test.utils.TestData.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UserServiceTest {

    private final UserPersistenceService userPersistenceService = mock(UserPersistenceService.class);
    private final PasswordEncoder encoder = mock(PasswordEncoder.class);

    private final UserService userService = new UserService(userPersistenceService, encoder);

    @BeforeEach
    public void setup() {
        when(encoder.encode(PASSWORD)).thenReturn(ENCODED_PASSWORD);
    }

    @Test
    void GetUser_NoIssues_ReturnsUser() {
        Principal principal = mock(Principal.class);
        User user = mock(User.class);

        when(principal.getName()).thenReturn(ADMIN_USERNAME);
        when(userPersistenceService.getUser(ADMIN_USERNAME)).thenReturn(Optional.of(user));

        Optional<User> userReturned = userService.getUser(principal);

        assertThat(userReturned).contains(user);
    }

    @Test
    void RegisterUser_NoIssues_ReturnsUser() {
        User userInput = UserBuilder.sample().id(null).build();
        User userToSave = UserBuilder.sample().id(null).encode().build();
        User userSaved = UserBuilder.sample().encode().build();

        when(userPersistenceService.saveUser(userToSave)).thenReturn(userSaved);

        User userReturned = userService.register(userInput);

        assertThat(userReturned).isEqualTo(userSaved);
    }

    @Test
    void RegisterUser_DuplicatedUser_ThrowsException() {
        User userInput = UserBuilder.sample().id(null).build();

        when(userPersistenceService.existsUser(ADMIN_USERNAME)).thenReturn(true);

        var exc = assertThrows(UserDuplicateException.class, () -> userService.register(userInput));
        assertThat(exc.getMessage()).isEqualTo(DUPLICATE_ADMIN_USER_MESSAGE);
    }
}
