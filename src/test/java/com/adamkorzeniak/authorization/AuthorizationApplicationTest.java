package com.adamkorzeniak.authorization;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@ActiveProfiles("test")
class AuthorizationApplicationTest {

    @Test
    void contextLoads() {
        assertTrue(true);
    }

}
