package com.adamkorzeniak.authorization.infrastructure.persistence;

import com.adamkorzeniak.authorization.domain.model.User;
import com.adamkorzeniak.authorization.infrastructure.persistence.mapper.UserEntityMapper;
import com.adamkorzeniak.utils.security.infrastructure.persistence.UserRepository;
import com.adamkorzeniak.utils.security.infrastructure.persistence.model.UserEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserPersistenceService {

    private final UserRepository userRepository;
    private final UserEntityMapper mapper;

    public boolean existsUser(String name) {
        return userRepository.existsByUsername(name);
    }

    public Optional<User> getUser(String name) {
        return userRepository.findByUsername(name)
                .map(mapper::mapToDomain);
    }

    public User saveUser(User user) {
        UserEntity entity = mapper.mapToEntity(user);
        UserEntity savedEntity = userRepository.save(entity);
        return mapper.mapToDomain(savedEntity);
    }

}
