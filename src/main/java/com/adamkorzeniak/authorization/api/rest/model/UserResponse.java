package com.adamkorzeniak.authorization.api.rest.model;

import lombok.Generated;
import lombok.Value;

@Value
@Generated
public class UserResponse {
    String username;
    String role;
}
