package com.adamkorzeniak.authorization.api.rest.mapper;

import com.adamkorzeniak.authorization.api.rest.model.UserRequest;
import com.adamkorzeniak.authorization.api.rest.model.UserResponse;
import com.adamkorzeniak.authorization.domain.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface UserApiMapper {

    UserResponse mapResponse(User user);

    @Mapping(target = "role", source = "role", defaultValue = "USER")
    @Mapping(target = "enabled", constant = "true")
    User mapRequest(UserRequest dto);

}
