package com.adamkorzeniak.authorization.api.rest;

import com.adamkorzeniak.authorization.api.rest.mapper.UserApiMapper;
import com.adamkorzeniak.authorization.api.rest.model.UserRequest;
import com.adamkorzeniak.authorization.api.rest.model.UserResponse;
import com.adamkorzeniak.authorization.domain.UserService;
import com.adamkorzeniak.authorization.domain.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Optional;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/api/user")
class UserController {

    private final UserService userService;
    private final UserApiMapper userApiMapper;

    @GetMapping("/me")
    public ResponseEntity<UserResponse> getMyDetails(Principal principal) {
        return Optional.ofNullable(principal)
                .flatMap(userService::getUser)
                .map(userApiMapper::mapResponse)
                .map(response -> new ResponseEntity<>(response, HttpStatus.OK))
                .orElse(new ResponseEntity<>(HttpStatus.NO_CONTENT));
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/register")
    public ResponseEntity<UserResponse> registerUser(@RequestBody @Valid UserRequest request) {
        User user = userService.register(userApiMapper.mapRequest(request));
        return new ResponseEntity<>(userApiMapper.mapResponse(user), HttpStatus.CREATED);
    }
}
