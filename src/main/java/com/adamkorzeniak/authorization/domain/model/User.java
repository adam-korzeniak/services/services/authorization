package com.adamkorzeniak.authorization.domain.model;

import com.adamkorzeniak.utils.security.infrastructure.model.Role;
import lombok.Data;
import lombok.Generated;

@Data
@Generated
public class User {
    private String username;
    private String password;
    private Role role;
    private boolean enabled;
}
