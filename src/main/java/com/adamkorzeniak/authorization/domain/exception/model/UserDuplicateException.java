package com.adamkorzeniak.authorization.domain.exception.model;

import com.adamkorzeniak.utils.exceptions.model.exception.BusinessException;

public class UserDuplicateException extends BusinessException {

    private static final String TITLE = "Username already taken";
    private static final String MESSAGE = "User with username %s already exists";

    public UserDuplicateException(String username) {
        super(String.format(MESSAGE, username));
    }

    @Override
    public String getCode() {
        return ErrorCode.USER_DUPLICATE_EXCEPTION.getCode();
    }

    @Override
    public String getTitle() {
        return TITLE;
    }
}
