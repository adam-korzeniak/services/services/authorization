package com.adamkorzeniak.authorization.domain.exception.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCode {
    USER_DUPLICATE_EXCEPTION("AUTH__USER_DUPLICATE");

    private final String code;
}
